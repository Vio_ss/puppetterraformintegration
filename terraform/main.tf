# variable "project-id" {
#     type = string
# }

# locals {
#   project_id = var.project-id
# }

resource "google_compute_address" "example_ip" {
  name = "example-ip"
  region = "us-central1"  # Change to your desired region
}

provider "google" {
  project = "gcpter2"
  region  = "us-central1"
  zone    = "us-central1-c"
}

resource "google_project_service" "compute_service" {
  project = "gcpter2"
  service = "compute.googleapis.com"
}

resource "google_compute_network" "vpc_network" {
  name                    = "example-network"
  auto_create_subnetworks = false
  delete_default_routes_on_create = true
  depends_on = [
    google_project_service.compute_service
  ]
}

resource "google_compute_subnetwork" "private_network" {
  name          = "example-subnet"
  ip_cidr_range = "10.2.0.0/16"
  network       = google_compute_network.vpc_network.self_link
}

resource "google_compute_router" "router" {
  name    = "quickstart-router"
  network = google_compute_network.vpc_network.self_link
}

resource "google_compute_router_nat" "nat" {
  name                               = "quickstart-router-nat"
  router                             = google_compute_router.router.name
  region                             = google_compute_router.router.region
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
}

resource "google_compute_route" "private_network_internet_route" {
  name             = "private-network-internet"
  dest_range       = "0.0.0.0/0"
  network          = google_compute_network.vpc_network.self_link
  next_hop_gateway = "default-internet-gateway"
  priority    = 100
}

# resource "google_compute_firewall" "allow_ssh" {
#   name    = "allow-ssh"
#   network = google_compute_network.vpc_network.name

#   allow {
#     protocol = "tcp"
#     ports    = ["22"]
#   }

#   source_ranges = ["0.0.0.0/0"]  # Allow traffic from any source

#   target_tags = ["ssh"]  # Apply the firewall rule to instances with the "ssh" tag
# }

resource "google_compute_instance" "vm_instance" {
  name         = "demo-instance"
  machine_type = "n1-standard-1"

  tags = ["demo-instance"]

  boot_disk {
    initialize_params {
      image = "ubuntu-os-pro-cloud/ubuntu-pro-1804-bionic-v20240223"
    }
  }
  network_interface {
    network = google_compute_network.vpc_network.self_link
    subnetwork = google_compute_subnetwork.private_network.self_link    
    access_config {} 
  }
  metadata = {
    puppet_master_ip = "192.168.29.32"
    puppet_client_ip = "192.168.29.151"
  }
  # provisioner "local-exec" {
  #   command = "ssh swes_siva@10.2.0.4 'sudo puppet agent -t'"
  # }
}

resource "null_resource" "trigger_puppet_agent" {
  depends_on = [google_compute_instance.vm_instance]

  connection {
    host        = "192.168.29.32"
    type        = "ssh"
    user        = "root"  # SSH user is root
    private_key = file("D:/worldline/gcloudkey")  # Path to the private key file
  }

  provisioner "remote-exec" {
    inline = [
      "ssh -i D:/worldline/gcloudkey root@192.168.29.32 'sudo puppet agent -t'",
      "ssh -i D:/worldline/gcloudkey root@192.168.29.151 'sudo puppet agent -t'",
    ]
  }
}





