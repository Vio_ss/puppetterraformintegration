# site.pp

node default {
  # Configure Puppet client to connect to the Puppet master server
  class { 'puppet::agent':
    server => '192.168.29.32',
  }

  # Command to trigger Puppet agent run on Puppet server
  exec { 'trigger_puppet_agent':
    command     => 'sudo puppet agent -t',
    refreshonly => true,
    subscribe   => Service['puppet'],
  }
}
